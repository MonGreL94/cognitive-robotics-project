#! /usr/bin/env python
from niryo_one_python_api.niryo_one_api import *
import rospy
import rospkg
from std_msgs.msg import String, Float32MultiArray
import numpy
import json
from collections import OrderedDict
import os


SYNC_MSG = "SYNC"
ACK_MESSAGE = "ACK"
NACK_MESSAGE = "NACK"


class trajectory_communication:
    __slots__ = 'verbose_publisher', 'command_topic', 'niryo', 'routines', 'responses', 'sync'

    def __init__(self, node_name, command_topic, verbose_topic, routines, responses, ros_ip, ros_master_uri, velocity=100):
        os.environ["ROS_IP"] = ros_ip
        os.environ["ROS_MASTER_URI"] = ros_master_uri
        try:
            self.verbose_publisher = rospy.Publisher(
                verbose_topic, Float32MultiArray, queue_size=1000)
            rospy.init_node(node_name, anonymous=True)
            rospy.Subscriber(command_topic, Float32MultiArray,
                             self.trajectory_callback)
            self.command_topic = command_topic
            self.niryo = NiryoOne()
            self.niryo.set_arm_max_velocity(velocity)
            self.sync = False
            with open(routines, 'r') as f:
                self.routines = json.load(f)
            with open(responses, 'r') as f:
                self.responses = json.load(f)
            self.sync_with_talker()
            rospy.loginfo("========== INITIALIZATION SUCCESS ==========")
        except rospy.ROSInterruptException as e:
            rospy.loginfo("=========== INITIALIZATION ERROR ===========")

    def sync_with_talker(self):
        while True:
            try:
                received = rospy.wait_for_message(
                    self.command_topic, Float32MultiArray, 2)
                if int(self.responses[SYNC_MSG]) == received.data[0]:
                    break
            except rospy.ROSException:
                pass
        self.publish("SYNC")
        self.sync = True
        rospy.loginfo('=============== SYNC SUCCESS ===============')

    def trajectory_callback(self, received):
        if self.sync:
            rospy.loginfo('============= COMMAND RECEIVED =============')
            response = self.exec_command(received.data)
            response = ACK_MESSAGE if response else NACK_MESSAGE
            pose = self.niryo.get_arm_pose()
            pose = [pose.position.x, pose.position.y, pose.position.z,
                    pose.rpy.roll, pose.rpy.pitch, pose.rpy.yaw]
            joints = self.niryo.get_joints()
            self.publish(response, pose+joints)
            rospy.loginfo('=============== VERBOSE SENT ===============')

    def publish(self, response, values=[]):
        msg = Float32MultiArray()
        msg.data = [int(self.responses[response])]
        msg.data.extend(numpy.round(values, 3).tolist())
        rospy.loginfo("RESPONSE > " + response)
        rospy.loginfo("POSE > " + str(msg.data[1:7]))
        rospy.loginfo("JOINTS > " + str(msg.data[7:]))
        self.verbose_publisher.publish(msg)

    def check_command(self, command, values):
        pass

    def exec_command(self, cmd):
        try:
            function_name = self.routines[int(cmd[0])]
            if function_name == "move_joints":
                params = str(numpy.around(cmd[1], 3).tolist())
            else:
                params = ", ".join((str(numpy.around(param, 3))
                                    for param in cmd[1:]) if cmd[1:] else "")
            command = "self.niryo.{0}({1})".format(function_name, params)
            eval(command)
            return True
        except NiryoOneException as e:
            return False

    def index_command(self, command):
        return list(self.commands.keys()).index(command)

    def get_command_from_index(self, index):
        return list(self.commands.keys())[index]

    # @classmethod
    # def calculate_distance(cls, x, y, z, x0=0, y0=0, z0=0):
    #     return numpy.linalg.norm(numpy.array((x, y, z)) - numpy.array((x0, y0, z0)))

    # DISTANCE_LIMIT = 0.6
    # @classmethod
    # def check_pose_limit(cls, x, y, z, x_y_z_limit=DISTANCE_LIMIT):
    #     distance = cls.calculate_distance(x, y, z)
    #     if distance <= x_y_z_limit:
    #         return "DISTANCE FROM BASE: {0:.3f}m".format(distance)
    #     return "EXCEED MAX DISTANCE: {0:.3f}m INSERTED: {1:.3f}m".format(x_y_z_limit, distance)


def run():
    rospack = rospkg.RosPack()
    path = os.path.join(rospack.get_path('trajectory_listener_pkg'), 'scripts')

    trasmission_topic = 'robotic_trajectory'
    verbose_topic = 'robotic_trajectory_verbose'
    node_name = 'trajectory_listener_node'

    routines = 'niryo_robot_routines.cfg'
    responses = 'niryo_responses.cfg'

    routines = os.path.join(path, routines)
    responses = os.path.join(path, responses)

    ros_ip = "10.42.0.96"
    ros_master_uri = "http://10.42.0.96:11311"
    trajectory_communication(node_name, trasmission_topic,
                             verbose_topic, routines, responses, ros_ip, ros_master_uri)
    rospy.spin()


if __name__ == '__main__':
    run()
