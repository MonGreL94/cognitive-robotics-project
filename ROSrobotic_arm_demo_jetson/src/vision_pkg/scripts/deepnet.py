import tensorflow as tf
import cv2 as cv


class DeepNet:

    __slots__ = "classNames","graph_path","session"

    def __init__(self, graph_path):
        self.classNames = {0: 'background', 1: 'red', 2: 'blue', 3: 'green', 4: 'white', 5: 'red_box', 6: 'blue_box', 7: 'green_box', 8: 'white_box'}
        self.graph_path = graph_path
        self.session = self.init_session()

    def init_session(self):
        # Read the graph.
        with tf.gfile.FastGFile(self.graph_path, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
        
        sess = tf.Session()
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')

        return sess

    def get_session(self):
        return self.session

    def id_class_name(self,class_id, classes):
        for key, value in classes.items():
            if class_id == key:
                return value

    def compute_image(self,color_image):
        # Run the model
        if self.session is not None:

            rows = color_image.shape[0]
            cols = color_image.shape[1]

            # color_image_yuv = cv.cvtColor(color_image, cv.COLOR_BGR2YUV)
            # color_image_yuv[: , : , 0] = cv.equalizeHist(color_image_yuv[: , : , 0])
            # color_image = cv.cvtColor(color_image_yuv, cv.COLOR_YUV2BGR)

            inp = cv.resize(color_image, (300, 300))
            inp = inp[:, :, [2, 1, 0]]  # BGR2RGB

            out = self.session.run([self.session.graph.get_tensor_by_name('num_detections:0'),
                            self.session.graph.get_tensor_by_name('detection_scores:0'),
                            self.session.graph.get_tensor_by_name('detection_boxes:0'),
                            self.session.graph.get_tensor_by_name('detection_classes:0')],
                            feed_dict={'image_tensor:0': inp.reshape(1, inp.shape[0], inp.shape[1], 3)})

            # Visualize detected bounding boxes.
            num_detections = int(out[0][0])
            centers = []
            net_output = []
            for i in range(num_detections):
                classId = int(out[3][0][i])
                class_name = self.id_class_name(classId, self.classNames)
                bbox = [float(v) for v in out[2][0][i]]
                score = float(out[1][0][i])
                if score > 0.7:
                    x = bbox[1] * cols
                    y = bbox[0] * rows
                    right = bbox[3] * cols
                    bottom = bbox[2] * rows
                    cv.rectangle(color_image, (int(x), int(y)), (int(right), int(bottom)), (125, 255, 51), thickness=2)
                    cv.putText(color_image, class_name, (int(x), int(y)), cv.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 255))
                    net_output.append(classId)
                    net_output.append(int(x))
                    net_output.append(int(y))
                    net_output.append(int(right))
                    net_output.append(int(bottom))
                    
            return color_image, net_output