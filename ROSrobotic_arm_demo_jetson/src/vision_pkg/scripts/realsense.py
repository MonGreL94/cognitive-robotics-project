import pyrealsense2 as rs
import numpy as np

BOX_AREA_LIMIT = 16000


class RealSense:

    __slots__ = "net","width_depth","height_depth","width_color","height_color","depth_fps","color_fps","decimate_factor","filters","decimator","pipeline","pc","colorizer"

    def __init__(self,width_depth,height_depth,depth_fps,width_color,height_color,color_fps,decimate_factor):
        self.depth_fps = depth_fps
        self.color_fps = color_fps
        self.width_depth = width_depth
        self.height_depth = height_depth
        self.width_color = width_color
        self.height_color = height_color
        self.decimate_factor = decimate_factor
        self.filters = [rs.disparity_transform(), rs.spatial_filter(), rs.temporal_filter(), rs.disparity_transform(False)]
        self.start_pipeline()

    # Initilize pipeline for captuer camera streams
    # Do it when you change some configuration parameters
    def start_pipeline(self):
        #self.stop_pipeline()
        self.pipeline = rs.pipeline()
        config = rs.config()
        config.enable_stream(rs.stream.depth, self.width_depth, self.height_depth, rs.format.z16, self.depth_fps)
        config.enable_stream(rs.stream.color, self.width_color, self.height_color, rs.format.bgr8, self.color_fps)
        self.pipeline.start(config)
        self.pc = rs.pointcloud()
        self.decimator = rs.decimation_filter()
        self.decimator.set_option(rs.option.filter_magnitude, 2 ** self.decimate_factor)
        self.colorizer = rs.colorizer()

    # Calculate the means value of coordinates, on the matrix 4x4 centered in the pixel
    def calculate_mean(self,verts, x, y):
        deltas = [(0,0),(-1,0),(1,0),(0,-1),(0,1),(-1,-1),(-1,1),(1,-1),(1,1),(-2,0),(-2,1),(-2,2),(-1,2),(0,2),(1,2),(2,2),(2,1),(2,0),(2,-1),(2,-2),(1,-2),(0,-2),(-1,-2),(-2,-2),(-2,-1)]
        N = len(deltas)
        X_mean = 0
        Y_mean = 0
        Z_mean = 0

        for d in deltas:
            cloud_point = verts[x+d[0]][y+d[1]]
            X_mean += cloud_point[0]
            Y_mean += cloud_point[1]
            Z_mean += cloud_point[2]
        
        X_mean = X_mean / N
        Y_mean = Y_mean / N
        Z_mean = Z_mean / N
        return X_mean, Y_mean, Z_mean
    
    # Return the coordinates of the assigned points
    def get_cordinates(self, mapped_frame, aligned_depth_frame, output):
        result = []
        num_of_ele = len(output)
        boxes = []
        balls = []
        if num_of_ele != 0:
            points = self.pc.calculate(aligned_depth_frame)
            self.pc.map_to(mapped_frame)
            verts = np.asarray(points.get_vertices(2)).reshape(480, 640, 3)
            texcoords = np.asarray(points.get_texture_coordinates(2))
            for delta in range(0,num_of_ele,5):
                identifier = output[delta]
                if identifier in range(1,5):
                    balls.append([identifier,output[delta+1],output[delta+2],output[delta+3],output[delta+4]])
                else:
                    boxes.append([identifier,output[delta+1],output[delta+2],output[delta+3],output[delta+4]])
            boxes_added = False
            for ball in balls:
                ball_remove = False
                for box in boxes:
                    if not boxes_added:
                        x_side = box[3]-box[1]
                        y_side = box[4]-box[2]
                        if BOX_AREA_LIMIT < x_side * y_side:
                            center_x = int(box[1] + (x_side/2))
                            center_y = int(box[2] + (y_side/2))
                            print("AREA : " + str(x_side*y_side))

                            X_mean, Y_mean, Z_mean = self.calculate_mean(verts,center_y,center_x)
                            
                            if Z_mean != 0:
                                result.append(float(box[0]))
                                result.append(X_mean)
                                result.append(Y_mean)
                                result.append(Z_mean)
                    if ball_in_the_box(box, ball):
                        ball_remove = True # balls.remove(ball)
                boxes_added = True
                if not ball_remove:
                    center_x = int(ball[1]+((ball[3]-ball[1])/2))
                    center_y = int(ball[2]+((ball[4]-ball[2])/2))
                    X_mean, Y_mean, Z_mean = self.calculate_mean(verts,center_y,center_x)
                    if Z_mean != 0:
                        result.append(float(ball[0]))
                        result.append(X_mean)
                        result.append(Y_mean)
                        result.append(Z_mean)                      
                    
                
            """
            for i in range(num_of_ele//5):
                    
                    delta = (i-1)*5
                    identifier = output[delta]
                    x = output[1+delta]
                    y = output[2+delta]
                    right = output[3+delta]
                    bottom = output[4+delta]
                    
                    center_x = int(x+((right-x)/2))
                    center_y = int(y+((bottom-y)/2))

                    X_mean, Y_mean, Z_mean = self.calculate_mean(verts,center_y,center_x)
                    if Z_mean != 0:
                        result.append(float(output[delta]))
                        result.append(round(X_mean,3))
                        result.append(round(Y_mean,3))
                        result.append(round(Z_mean,3))
        """
        return result

    # Return color image and depth frame
    def get_images(self):
        frames = self.pipeline.wait_for_frames()
        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()

        depth_frame = self.decimator.process(depth_frame)

        for f in self.filters:
            depth_frame = f.process(depth_frame)

        if not depth_frame or not color_frame:
            return

        depth_intrinsics = rs.video_stream_profile(depth_frame.profile).get_intrinsics()
        w, h = depth_intrinsics.width, depth_intrinsics.height

        color_image = np.asanyarray(color_frame.get_data())
        #depth_image = np.asanyarray(depth_frame.get_data())

        image_height, image_width, _ = color_image.shape
        colorized_depth = self.colorizer.colorize(depth_frame)

        depth_colormap = np.asanyarray(colorized_depth.get_data())

        mapped_frame, color_source = colorized_depth, depth_colormap

        # Create alignment primitive with color as its target stream:
        align = rs.align(rs.stream.color)
        frameset = align.process(frames)

        # Update color and depth frames:
        aligned_depth_frame = frameset.get_depth_frame()

        return color_image, depth_frame , aligned_depth_frame, mapped_frame #,depth_image


def ball_in_the_box(box, ball):

    # x_box, y_box, w_box, h_box = box
    # x_ball, y_ball, w_ball, h_ball = ball
    #if (x_ball >= x_box) and ((x_ball + w_ball) <= (x_box + w_box)) and (y_ball >= y_box) and ((y_ball + h_ball) <= (y_box + h_box)):

    _, x_min_box, y_min_box, x_max_box, y_max_box = box
    _, x_min_ball, y_min_ball, x_max_ball, y_max_ball = ball
    if x_min_ball >= x_min_box and x_max_ball <= x_max_box and y_min_ball >= y_min_box and y_max_ball <= y_max_box:
        return True
    else:
        return False

