#!/usr/bin/env python3
import roslib
import sys
import rospy
import rospkg
import cv2 as cv
import numpy as np
import time
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import String, Int32MultiArray, Float32MultiArray
from realsense import RealSense
from deepnet import DeepNet
import os

if __name__ == "__main__":

    # NOTA BENE
    # Bisogna cambiare il path del grafo in base alla cartella in cui si trova

    rospack = rospkg.RosPack()
    path = rospack.get_path('vision_pkg')
    ssd_mobilenet = DeepNet(path + '/resources/frozen_inference_graph.pb')

    real_camera = RealSense(rospy.get_param('depth_width'),rospy.get_param('depth_height'),rospy.get_param('depth_fps'),rospy.get_param('color_width'),rospy.get_param('color_height'),rospy.get_param('color_fps'),rospy.get_param('decimate_factor'))

    ros_ip = "10.42.0.243"
    ros_master_uri = "http://10.42.0.96:11311"
    os.environ["ROS_IP"] = ros_ip
    os.environ["ROS_MASTER_URI"] = ros_master_uri

    image_pub = rospy.Publisher('image_topic', CompressedImage, queue_size= 10)
    net_pub = rospy.Publisher('net_topic', Int32MultiArray, queue_size=10)
    coordinates_pub = rospy.Publisher('cloudpoint_topic', Float32MultiArray, queue_size=10)

    rospy.init_node('image_processed', anonymous=True)
    rate = rospy.Rate(10) # 100hz

    while not rospy.is_shutdown():
        init_time = int(round(time.time() * 1000))
        color_image, depth_frame, aligned_depth_frame, mapped_frame = real_camera.get_images()
        processed_image, net_output = ssd_mobilenet.compute_image(color_image)
        coordinates = real_camera.get_cordinates(mapped_frame,aligned_depth_frame,net_output)

        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"
        msg.data = np.array(cv.imencode('.jpg',processed_image)[1]).tostring()

        image_pub.publish(msg)
        image_time = int(round(time.time() * 1000))
        rospy.loginfo(" IMAGE SENDED... ( TIME = {0} )".format(image_time - init_time))
        
        # Manca il publisher delle coordinate
        #print(coordinates)
        coordinates_msg = Float32MultiArray(data=coordinates)
        output_msg = Int32MultiArray(data=net_output)

        #cv.imshow('ORIGINAL IMAGE',processed_image)
        #if cv.waitKey(1) == ord('q'):
        #    break

        #net_pub.publish(output_msg)
        #rospy.loginfo(" NET OUTPUT SENDED...")

        coordinates_pub.publish(coordinates_msg)
        # cloudpoint_time = int(round(time.time() * 1000))
        # rospy.loginfo(" COORDINATES SENDED... ( TIME = {0} )".format(cloudpoint_time - init_time))

        # rospy.loginfo(" \n###########################\n")

        rate.sleep()
