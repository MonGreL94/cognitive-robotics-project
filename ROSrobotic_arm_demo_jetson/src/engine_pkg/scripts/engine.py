#! /usr/bin/env python
import rospkg
import rospy
from std_msgs.msg import Int32MultiArray, Float32MultiArray
import copy
from utilities_lib import homogeneous_transformation
import numpy as np
import os
import math
import time
import copy

ENGINE_CMD = {
    "scan_pose": 0,
    "automove": 1,
    "automove_fixed_space": 2,
    "open_gripper": 3,
    "close_gripper": 4,
    "move_scan": 5,
    "get_info": 6
}

OBJECTS = {
    1: 'red_ball',
    2: 'blue_ball',
    3: 'green_ball',
    4: 'white_ball',
    5: 'red_box',
    6: 'blue_box',
    7: 'green_box',
    8: 'white_box'
}

BALL_HEIGHT = 0.02
BOX_HEIGHT = 0.08
SCAN_HEIGHT = 0.28

MAX_VISION_OCC = 4
MAX_VISION_COUNTER = 6

PARTITIONS = 6 # 8

CORRECTION_LIMIT = 0.03
JOINT_OFFSET = 0.07

# pnt_cld_gripper=np.array([0.005, 0.037, 0.17]
def compute_error_grab(ee_pos, actual_rotation_angle, pnt_cld_ball, pnt_cld_gripper=np.array([0.002, 0.034, 0.17])):
    absolute_pos_gripper = np.asarray(homogeneous_transformation(ee_pos, pnt_cld_gripper, actual_rotation_angle))
    absolute_pos_ball = np.asarray(homogeneous_transformation(ee_pos, pnt_cld_ball, actual_rotation_angle))
    return absolute_pos_ball - absolute_pos_gripper


def yn_input(req_msg, error_msg="Accepted answers: 'Y, N, y, n.'"):
    accepted_chars = ["Y", "y", "N", "n"]
    response = raw_input(req_msg)
    while response not in accepted_chars:
        response = input(" ".join((error_msg, req_msg)))
    return response in accepted_chars[:2]


def distance(starting_point, ending_point = [0,0]):
    return np.linalg.norm(np.array(ending_point)[:2] - np.array(starting_point)[:2])

def nearest_target(clouds, identifier):
    target_cloud = None
    for x in range(0, len(clouds), 4):
        distance_target = distance([clouds[x+1], clouds[x+2]])
        if clouds[x] == identifier and (target_cloud is None or distance_target < target_cloud[3]):
            target_cloud = list(clouds[x+1 : x+4]) + [distance_target]
    return None if target_cloud is None else target_cloud[:3]


def shortest_path(balls, boxes, ee_pos, delta=4):
    # balls = [[[x,y,z], id], [[x,y,z], id]]
    # boxes = [[[x,y,z], id], [[x,y,z], id]]
    distance_ee_ball = []
    distance_ball_box = []
    minimum_target = None
    for ball in balls:
        ball_pos, ball_id = ball[0], ball[1]
        distance_ee_ball = distance(ee_pos, ball_pos)
        for box in boxes:
            box_pos, box_id = box[0], box[1]
            if box_id == (ball_id + delta):
                distance_ball_box = distance(box_pos, ball_pos)
                total_distance = distance_ee_ball + distance_ball_box
                if minimum_target is None or (minimum_target[-1] > total_distance):
                    minimum_target = [ball_id, ball_pos,
                                      box_id, box_pos, total_distance]
    return minimum_target if minimum_target is None else minimum_target[:4] # [ball_id, [x, y, z], box_id, [x, y, z]]


def absolute_scene(points, ee_pos, rotation_angle):
    # [[[x,y,z],id], [[x,y,z],id]]
    absolutePoints = []
    for point in points:
        absolutePoints.append([homogeneous_transformation(ee_pos, list(point[:2]) + [0], rotation_angle), point[2]])
    return absolutePoints


class Engine:

    __slots__ = 'engine_pub', 'current_pose', 'current_joints', 'operation_success', 'scene', 'counter', 'update_scene', 'balls', 'boxes', 'res_engine_topic', 'cloudpoints', 'sector'

    def __init__(self, node_name, engine_topic, res_engine_topic, cloudpoint_topic, net_topic, ros_ip, ros_master_uri):
        os.environ["ROS_IP"] = ros_ip
        os.environ["ROS_MASTER_URI"] = ros_master_uri
        self.engine_pub = rospy.Publisher(engine_topic, Float32MultiArray, queue_size=100)
        self.res_engine_topic = res_engine_topic
        rospy.init_node(node_name, anonymous=True)
        rospy.Subscriber(res_engine_topic, Float32MultiArray,self.operation_result_callback)
        self.operation_success = False
        self.current_pose = None
        self.current_joints = None
        self.publish_operation("get_info")
        rospy.loginfo("POSE > " + str(self.current_pose))
        rospy.loginfo("JOINTS > " + str(self.current_joints))
        self.scene = {}
        self.counter = 0
        self.update_scene = False
        self.boxes = list()
        self.balls = list()
        self.cloudpoints = None
        self.sector = 0
        rospy.Subscriber(cloudpoint_topic, Float32MultiArray,self.cloudpoint_callback)
        rospy.Subscriber(net_topic, Int32MultiArray, self.net_callback)


    def eagle_eye(self):
        # self.sector = 1 if self.sector >= PARTITIONS else self.sector+1
        self.publish_operation("scan_pose", [PARTITIONS, self.sector ,0.27, 0.27])
        self.publish_operation("get_info")
        self.start_update_scene()
    
    def update_object(self, primitive, new_objects, high_precision=True):
        if high_precision:
            local_primitive = list()
            for element in primitive:
                local_primitive.append([np.around(element[0], 2).tolist(), element[1]])
            for element in new_objects:
                local_element = [np.around(element[0], 2).tolist(), element[1]]
                if local_element not in local_primitive:
                    primitive.append(element)
        else:
            for element in new_objects:
                if element not in primitive:
                    primitive.append(element)


    def start_engine(self):
        rospy.sleep(2)
        self.publish_operation("get_info")
        while self.current_joints is None or self.current_pose is None:
            rospy.sleep(0.5)
            rospy.loginfo("WAIT FOR INFO")
        self.publish_operation("automove_fixed_space", [self.current_pose[0], self.current_pose[1], 0.2])
        while True:
            if self.sector >= PARTITIONS:
                if not yn_input("REPEAT START ENGINE? > "):
                    break
                self.sector = 1
            else:
                self.sector += 1
            print(self.sector)
            self.eagle_eye()
            rospy.loginfo("===== EAGLE EYE =====")
            while self.boxes and self.balls:
                # [ball_id, ball_pos, box_id, box_pos, distance]
                start_time = time.time()
                target = shortest_path(self.balls, self.boxes, self.current_pose[:3])
                rospy.loginfo("ELAPSED TIME : " + str(time.time() - start_time))
                if target is None:
                    rospy.loginfo(">>>>>>>>>>>> NO TARGET <<<<<<<<<<<<")
                    break
                else:
                    ball_pos = target[1][:2] + [BALL_HEIGHT]
                    self.publish_operation("open_gripper", [500])
                    self.publish_operation("automove_fixed_space", [ball_pos[0], ball_pos[1], 0.08])
                    self.publish_operation("automove_fixed_space", [ball_pos[0], ball_pos[1], 0.03]) #0.005])
                    self.publish_operation("get_info")
                    clouds = copy.deepcopy(self.cloudpoints)
                    target_ball_cloud = nearest_target(clouds, target[0])
                    if target_ball_cloud is None:
                        rospy.loginfo(" >>>>>>>> CORRECTION FAILED <<<<<<<<")
                        self.publish_operation("automove_fixed_space", [ball_pos[0], ball_pos[1], 0.08])
                    else:
                        correction = compute_error_grab(self.current_pose[0:3], self.current_joints[0], target_ball_cloud)
                        corrected_xy = (np.asarray(self.current_pose[:2]) + correction[:2]).tolist()
                        if distance(correction[:2]) > CORRECTION_LIMIT:
                            self.publish_operation("automove_fixed_space", corrected_xy + [0.03])
                        self.publish_operation("automove_fixed_space", corrected_xy + [0.005])
                        # corrected_pose_low = (np.asarray(self.current_pose[:2]) + correction[:2]).tolist() + [0.005]
                        # self.publish_operation("automove_fixed_space", corrected_pose_low)
                        self.publish_operation("close_gripper", [500])
                        box_pos = target[3][:2] + [BOX_HEIGHT]
                        self.publish_operation("automove_fixed_space", [ball_pos[0], ball_pos[1], 0.08])
                        self.publish_operation("automove_fixed_space", box_pos)
                        ###############################################
                        ## -> self.check_box(box_id)
                        # box_id = target[2]
                        # clouds = copy.deepcopy(self.cloudpoints)
                        # identifiers = [clouds[i] for i in range(0, len(clouds), 4)]
                        # if box_id not in identifiers:
                        #     self.boxes.remove([target[3], target[2]])
                        ###############################################
                        self.publish_operation("open_gripper", [500])
                        correction = compute_error_grab(self.current_pose[0:3], self.current_joints[0], target_ball_cloud)
                    rospy.loginfo("BALLS BEFORE REMOVE :\n" + str(self.balls))
                    self.balls.remove([target[1], target[0]])
                    rospy.loginfo("BALLS AFTER REMOVE :\n" + str(self.balls))
                    


    def divide_map(self, obj_map):
        boxes = []
        balls = []
        for obj in obj_map:
            if obj[1] in range(1, 5):
                balls.append(obj)
            else:
                boxes.append(obj)
        return balls, boxes

    def publish_operation(self, operation, params=[], loop=False, retry_on_failure=False):
        # operation : String
        self.operation_success = False
        cmd = Float32MultiArray()
        cmd.data = [int(ENGINE_CMD[operation])] + params
        rospy.loginfo("======= PUBLISH COMMAND ======")
        rospy.loginfo("COMMAND > " + operation)
        self.engine_pub.publish(cmd)
        while True:
            try:
                rospy.wait_for_message(self.res_engine_topic, Float32MultiArray, 1)
                # if retry_on_failure and not self.operation_success:
                #     rospy.loginfo("===== RE-PUBLISH COMMAND =====")
                #     self.engine_pub.publish(cmd)
                # else:
                #     return
                return
            except rospy.ROSException:
                if loop:
                    self.engine_pub.publish(cmd)
                else:
                    pass

    def operation_result_callback(self, received):
        self.operation_success = True if received.data[0] else False
        self.current_pose = received.data[1:7]
        self.current_joints = tuple([received.data[7] - JOINT_OFFSET] + list(received.data[8:]))
        #self.current_joints = received.data[7:]
        if self.operation_success:
            rospy.loginfo("====== OPERATION SUCCESS =====")
        else:
            rospy.loginfo("====== OPERATION FAILED ======")


    def cloudpoint_callback(self, data):
        self.cloudpoints = np.round(data.data, 3).tolist()
        if self.update_scene:
            size = len(self.cloudpoints)
            if size != 0:
                for i in range(0, size, 4):
                    identifier = self.cloudpoints[i]
                    x = self.cloudpoints[i+1]
                    y = self.cloudpoints[i+2]
                    z = self.cloudpoints[i+3]
                    self.check_object(identifier, x, y, z)
            self.counter += 1
            if self.counter == MAX_VISION_COUNTER:
                self.counter = 0
                self.update_scene = False
                self.scene = self.clean_scene()
                absolute_map = absolute_scene(self.scene, self.current_pose[0:3], self.current_joints[0])
                balls, boxes = self.divide_map(absolute_map)
                self.update_object(self.balls, balls, high_precision=True)
                self.update_object(self.boxes, boxes, high_precision=False)

    def net_callback(self, received):
        pass

    def check_object(self, identifier, x, y, z):
        min_tollerance, max_tollerance, delta_tollerance = (
            0.005, 0.005, 0.001) if identifier in range(1, 5) else (0.020, 0.030, 0.001)
        if self.scene:
            for X in np.arange(x-min_tollerance, x+max_tollerance, delta_tollerance):
                for Y in np.arange(y-min_tollerance, y+max_tollerance, delta_tollerance):
                    X = np.round(X, 3)
                    Y = np.round(Y, 3)
                    try:
                        self.scene[(X, Y, identifier)] = self.scene[(X, Y, identifier)] + 1
                        return
                    except KeyError:
                        pass
        self.scene[(x, y, identifier)] = 1


    def start_update_scene(self):
        self.scene = {}
        rospy.loginfo("======= UPDATING SCENE =======")
        self.update_scene = True
        while self.update_scene:
            rospy.sleep(0.1)
        rospy.loginfo("======== SCENE UPDATED =======")

    def is_updating(self):
        return self.update_scene

    def get_scene(self):
        return self.scene


    def clean_scene(self):
        cleaned_scene = list()
        for key, value in self.scene.items():
            if value >= MAX_VISION_OCC:
                cleaned_scene.append(key)
        return cleaned_scene


if __name__ == "__main__":
    ros_ip = "10.42.0.243"
    ros_master_uri = "http://10.42.0.96:11311"

    node_name = 'engine_node'
    engine_topic = 'engine_topic'
    res_engine_topic = 'res_engine_topic'
    cloudpoint_topic = 'cloudpoint_topic'
    net_topic = 'net_topic'

    engine = Engine(node_name, engine_topic, res_engine_topic,
                    cloudpoint_topic, net_topic, ros_ip, ros_master_uri)
    engine.start_engine()
