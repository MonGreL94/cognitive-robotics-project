from __future__ import print_function

import numpy as np
import math

def homogeneous_transformation(pos_base, pos_real_sense, rotation_angle):
    pos_base_col = np.transpose(pos_base)
    pos_real_sense_col = np.transpose(pos_real_sense)
    pos_real_sense_col[1]=-pos_real_sense_col[1]
    rotation_angle = rotation_angle - math.pi/2
    rotation_matrix_z = [[math.cos(rotation_angle),-math.sin(rotation_angle),0],[math.sin(rotation_angle),math.cos(rotation_angle),0],[0,0,1]]
    rotated_system=np.dot(rotation_matrix_z,pos_real_sense_col)
    result = pos_base_col + rotated_system
    return np.round(result,3).tolist()


def yaw_angle(x,y):
    return np.sign(y) * math.acos(x/math.hypot(x,y))

def yn_input(req_msg, error_msg="Accepted answers: 'Y, N, y, n.'"):
    accepted_chars = ["Y", "y", "N", "n"]
    response = raw_input(req_msg)
    while response not in accepted_chars:
        response = raw_input(" ".join((error_msg, req_msg)))
    return response in accepted_chars[:2]


def insert_number(msg, error_msg="Value Error", integer_value=False, lower_bound=None, upper_bound=None, bound_err_msg=None):
    while True:
        try:
            value = int(raw_input(msg)) if integer_value else float(
                raw_input(msg))
            if lower_bound is not None and value < lower_bound or upper_bound is not None and value > upper_bound:
                if bound_err_msg is None:
                    print(error_msg)
                else:
                    print(bound_err_msg)
            else:
                return value
        except ValueError:
            print(error_msg)


def print_human_commands(methods, cols=4):
    for i, m in zip(range(len(methods)), methods):
        print("{:<4} {:<25}".format("[{0}]".format(
            i), m), end="\n" if (i + 1) % cols == 0 else "")
    print("")


def try_index(msg, limit):
    try:
        index = int(raw_input(msg))
        if 0 <= index < limit:
            return index
        else:
            raise ValueError
    except ValueError:
        return None
