from __future__ import division
import math
import numpy as np


MAX_ROTATION = [[-175, 175],
                [-90, 36.7],
                [-80, 90],
                [-175, 175],
                [-100, 110],
                [-147.5, 147.5]]

DEC_LIMIT = 3


class CoordsBoundsError(Exception):
    pass


class JointsBoundsError(Exception):
    pass


class JointCalculator:

    __slots__ = "climx", "climy", "above_limz", "terrain_limz", "radius_limit", "len0", "len1", "joint4", "joint6", "end_effector_offset"

    def __init__(self, climx, climy, above_limz, terrain_limz, radius_limit, len0, len1, j4, j6):
        self.climx, self.climy = climx, climy
        self.above_limz, self.terrain_limz = above_limz, terrain_limz
        self.radius_limit = radius_limit
        self.len0, self.len1 = len0, len1
        self.joint4, self.joint6 = j4, j6

    def get_distance_origin(self, x, y, z):
        return np.linalg.norm(np.array((x, y, z)))

    def check_coords(self, x, y, z):
        #print(z)
        #return self.get_distance_origin(x, y, z) < self.radius_limit and (abs(x) > self.climx and abs(y) > self.climy and z > 0 or z > self.above_limz)
        cond1 = self.get_distance_origin(x, y, z) < self.radius_limit
        cond2 = (abs(x) > self.climx or abs(y) > self.climy) and z >= self.terrain_limz
        cond3 = z > self.above_limz
        return cond1 and (cond2 or cond3)


    def check_degrees(self, value, lower, upper, inclusive=False):
        lower = math.radians(lower)
        upper = math.radians(upper)
        # print(str(math.degrees(lower)) + " --- " +
        #       str(math.degrees(value)) + " ---- " + str(math.degrees(upper)))
        return lower <= value <= upper if inclusive else lower < value < upper

    def check_joints(self, j1, j2, j3, j5):
        return self.check_degrees(j1, MAX_ROTATION[0][0], MAX_ROTATION[0][1]) and self.check_degrees(j2, MAX_ROTATION[1][0], MAX_ROTATION[1][1]) and self.check_degrees(j3, MAX_ROTATION[2][0], MAX_ROTATION[2][1]) and self.check_degrees(j5, MAX_ROTATION[4][0], MAX_ROTATION[4][1])

    def joint_angles(self, x, y, z):
        #x=-x
        z += self.terrain_limz
        if not self.check_coords(x, y, z):
            raise CoordsBoundsError
        r, joint1 = self.top_down(x, y)
        joint2, joint3, joint5 = self.planar_angles(r, z)
        if not self.check_joints(joint1, joint2, joint3, joint5):
            raise JointsBoundsError
        return [np.around(joint1, DEC_LIMIT), np.around(joint2, DEC_LIMIT), np.around(joint3, DEC_LIMIT), np.around(self.joint4, DEC_LIMIT), np.around(joint5, DEC_LIMIT), np.around(self.joint6, DEC_LIMIT)]

    def top_down(self, x, y):
        r = math.hypot(x, y)
        return r, np.sign(y)*math.acos(x/r) #+ math.pi

    def planar_angles(self, r, z):
        dist = math.hypot(r, z)
        joint2 = math.acos((math.pow(self.len0, 2) + math.pow(dist, 2) -
                            math.pow(self.len1, 2)) / (2 * self.len0 * dist)) + math.atan(z/r) 
        joint3 = math.acos((math.pow(self.len0, 2) + math.pow(self.len1, 2) -
                            math.pow(dist, 2)) / (2 * self.len0 * self.len1))
        #print(math.degrees(joint2))
        #print(math.degrees(joint3))
        joint5 = math.pi/2 - joint2 - joint3
        joint2 -= math.pi/2
        joint3 -= math.pi/2
        return joint2, joint3, joint5



if __name__ == "__main__":
    # millimeters calculation
    climx = 160/2
    climy = 186/2
    above_limz = 220 # 60
    terrain_limz = -180
    radius_limit = 500
    len0, len1 = 220, 222
    joint4, joint6 = 0, 0.9
    jc = JointCalculator(climx, climy, above_limz,
                         terrain_limz, radius_limit, len0, len1, joint4, joint6)
    DEC_LIMIT = 3
    x, y, z = 200, 200, 200
    print(x, y, z)
    j1, j2, j3, j4, j5, j6 = jc.joint_angles(x, y, z)
    print(j1, j2, j3, j4, j5, j6)
    x, y, z = 0, 300, 200
    print(x, y, z)
    j1, j2, j3, j4, j5, j6 = jc.joint_angles(x, y, z)
    print(j1, j2, j3, j4, j5, j6)
    x, y, z = 300, 200, 200
    print(x, y, z)
    j1, j2, j3, j4, j5, j6 = jc.joint_angles(x, y, z)
    print(j1, j2, j3, j4, j5, j6)
    x, y, z = -200, 300, 200
    print(x, y, z)
    j1, j2, j3, j4, j5, j6 = jc.joint_angles(x, y, z)
    print(j1, j2, j3, j4, j5, j6)
    x, y, z = -200, -300, 200
    print(x, y, z)
    j1, j2, j3, j4, j5, j6 = jc.joint_angles(x, y, z)
    print(j1, j2, j3, j4, j5, j6)
    x, y, z = 150, 150, 10
    print(x, y, z)
    j1, j2, j3, j4, j5, j6 = jc.joint_angles(x, y, z)
    print(j1, j2, j3, j4, j5, j6)