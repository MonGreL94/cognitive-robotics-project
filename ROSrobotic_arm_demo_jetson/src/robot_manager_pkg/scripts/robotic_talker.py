#! /usr/bin/env python
import threading
import rospy
import rospkg
import json
from std_msgs.msg import String, Float32MultiArray
from utilities_lib import *
from inverse_kinematic.ik_solver import JointCalculator
from collections import OrderedDict
import os

GRIPPER_ID = 11
WAIT_VERBOSE_SLEEP_TIME = 1  # sec
WAIT_VERBOSE_LOG_TIME = 3  # sec
DEC_LIMIT = 3
GRIPPER_HEIGHT = 0.145 # 0.14  # 0.09
ACK_MESSAGE = "ACK"
NACK_MESAGE = "NACK"
SYNC_MSG = "SYNC"
CALIBRATION_REQ = ["NO calibration", "AUTO CALIBRATION", "MANUAL CALIBRATION"]
AUTO_ROLL = 0
AUTO_PITCH = 1.57#1.45#1.6#1.53#1.6
JOINT1_MIN = -3.053
JOINT1_MAX = 3.053
ENGINE_CMD = {
    0: "scan_pose",
    1: "automove",
    2: "automove_fixed_space",
    3: "open_gripper",
    4: "close_gripper",
    5: "move_scan",
    6: "get_info"
}


class RobotManager:
    __slots__ = 'command_pub', 'engine_pub', 'verbose', 'routines', 'pkt_cmd_buffer', 'received_verbose', 'current_pose', 'current_joints', 'sync', 'verbose_topic', 'responses'

    def __init__(self, node_name, command_topic, verbose_topic, engine_topic, res_engine_topic, routines, responses, ros_ip, ros_master_uri):
        os.environ["ROS_IP"] = ros_ip
        os.environ["ROS_MASTER_URI"] = ros_master_uri
        try:
            self.command_pub = rospy.Publisher(command_topic, Float32MultiArray, queue_size=100)
            self.engine_pub = rospy.Publisher(res_engine_topic, Float32MultiArray, queue_size=100)
            rospy.init_node(node_name, anonymous=True)
            rospy.Subscriber(verbose_topic, Float32MultiArray, self.verbose_callback)

            temp_eng_pub = rospy.Publisher(engine_topic, Float32MultiArray, queue_size=100)
            cmd = Float32MultiArray()
            cmd.data = [6]
            temp_eng_pub.publish(cmd)
            
            rospy.Subscriber(engine_topic, Float32MultiArray, self.engine_callback)

            self.verbose_topic = verbose_topic
            self.verbose = True
            self.received_verbose = None
            self.pkt_cmd_buffer = list()
            self.sync = False
            with open(routines, 'r') as f:
                self.routines = json.load(f)
            with open(responses, 'r') as f:
                self.responses = json.load(f)
            self.sync_with_listener()
            rospy.loginfo("========= INITIALIZATION COMPLETED =========")
            self.current_pose = None
            self.current_joints = None
            self.request_calibration()
        except rospy.ROSInterruptException:
            rospy.loginfo("=========== INITIALIZATION ERROR ===========")

    def set_joint_configuration(self, joint_configuration):
        with open(joint_configuration, 'r') as f:
            params = json.load(f)
            self.joint_calculator = JointCalculator(climx=params["climx"], climy=params["climy"], above_limz=params["above_limz"],
                                                    terrain_limz=params["terrain_limz"], radius_limit=params[
                                                        "radius_limit"], len0=params["len0"],
                                                    len1=params["len1"], j4=params["joint4"], j6=params["joint6"])

    def verbose_callback(self, received):
        if self.sync:
            rospy.loginfo("============= VERBOSE RECEIVED =============")
            response = self.responses[str(int(received.data[0]))]
            self.received_verbose = response
            if response == ACK_MESSAGE or response == NACK_MESAGE:
                self.current_pose = received.data[1:7]
                self.current_joints = received.data[7:]
            self.verbose = True

    def engine_callback(self, received):
        rospy.loginfo("============== ENGINE RECEIVED =============")
        if self.sync:
            rospy.loginfo("============ ENGINE CMD RECEIVED ===========")
            operation = ENGINE_CMD[received.data[0]]
            params = ", ".join(str(param) for param in received.data[1:])
            result = eval("self.{0}({1})".format(operation, params))
            to_engine = Float32MultiArray()
            to_engine.data = [1 if result else 0] + list(self.current_pose) + list(self.current_joints)
            self.engine_pub.publish(to_engine)

    def request_calibration(self):
        print_human_commands(CALIBRATION_REQ, 3)
        index = insert_number(
            "CALIBRATE? > ", integer_value=True, lower_bound=0, upper_bound=2)
        if index == 1:
            self.exec_routine("calibrate_auto")
        elif index == 2:
            self.exec_routine("calibrate_manual")
        else:
            self.exec_routine("get_arm_pose")

    def sync_with_listener(self):
        sync_msg = Float32MultiArray()
        sync_code = -1  # sync_code = int(self.responses[SYNC_MSG])
        sync_msg.data = [sync_code]
        while True:
            try:
                received = rospy.wait_for_message(
                    self.verbose_topic, Float32MultiArray, 2)
                if sync_code == received.data[0]:
                    break
                else:
                    self.command_pub.publish(sync_msg)
            except rospy.ROSException:
                self.command_pub.publish(sync_msg)
        self.sync = True
        rospy.loginfo('=============== SYNC SUCCESS ===============')

    def command_publish(self):
        if self.sync:
            cmd = Float32MultiArray()
            while not rospy.is_shutdown() and self.pkt_cmd_buffer or not self.verbose:
                slept_time = 0
                if self.verbose:
                    cmd.data = self.pkt_cmd_buffer.pop(0)
                    self.command_pub.publish(cmd)
                    self.verbose = False
                    rospy.loginfo(" > Wait for execution...")
                else:
                    rospy.sleep(WAIT_VERBOSE_SLEEP_TIME)
                    if slept_time >= WAIT_VERBOSE_LOG_TIME:
                        rospy.loginfo(" > Sleeping...")
                        slept_time = 0
                    else:
                        slept_time += 1
            rospy.loginfo("============ END COMMAND PUBLISH ===========")
            return self.received_verbose == ACK_MESSAGE
        else:
            return False

    def start_talk(self):
        rospy.loginfo("============= PUBLISH STARTED ==============")
        return self.command_publish()

    def terminate(self):
        rospy.loginfo("============== ROSPY SHUTDOWN ==============")
        exit()

    def add_cmd_pkt(self, cmd_pkt):
        self.pkt_cmd_buffer.append(cmd_pkt)

    def get_pose(self):
        return self.current_pose

    def get_joints(self):
        return self.current_joints

    ################################### DEMO ##################################

    def exec_demo(self, cfg_file):
        with open(cfg_file, 'r') as cfg_file:
            for h_cmd in json.load(cfg_file):
                self.add_cmd_pkt(
                    self.make_packet_from_name(h_cmd[0], h_cmd[1]))
        self.start_talk()
        self.terminate()

    ######################### COMMAND PACKAGE MAKING  #########################

    def make_packet_from_index(self, command_index, values):
        packet = [command_index]
        packet.extend(values)
        return packet

    def make_packet_from_name(self, command_name, values):
        packet = [self.routines.index(command_name)]
        packet.extend(values)
        return packet

    ######################### BEGIN USER PROCEDURES ###########################

    def user_interface(self, routines, loop_msg=None):
        with open(routines, 'r') as f:
            self.user_routines = json.load(f, object_pairs_hook=OrderedDict)
        while True:
            self.user_interface_single_shot(self.user_routines)
            if loop_msg is not None and not yn_input(loop_msg):
                self.terminate()

    def user_interface_single_shot(self, user_routines):
        request_msg = "Insert command: "
        command_error_msg = "Command not available. Retry: "
        print_human_commands(user_routines, 3)
        self.insert_command_index(
            self.routines, user_routines, request_msg, command_error_msg)
        self.start_talk()

    def insert_command_index(self, routines, user_routines, request_msg, command_error_msg):
        index = insert_number(request_msg, command_error_msg,integer_value=True, lower_bound=-1, upper_bound=len(user_routines), print_human=(user_routines, 3))
        user_command = list(user_routines.keys())[index]
        user_request = user_routines[user_command]["request"]
        routine = user_routines[user_command]["routine"]
        if routine is None:
            result = eval("self.{0}".format(user_request))()
            rospy.loginfo(result)
        else:
            routine_index = self.routines.index(routine)
            if user_request is None:
                self.add_cmd_pkt(
                    self.make_packet_from_index(routine_index, list()))
            else:
                self.add_cmd_pkt(self.make_packet_from_index(
                    routine_index, eval("self.{0}".format(user_request))()))

     ####################### USER PROCEDURES: REQUESTS #########################

    def pose_request(self):
        x = insert_number("Insert x: ")
        y = insert_number("Insert y: ")
        z = insert_number("Insert z: ")
        roll = insert_number("Insert roll: ")
        pitch = insert_number("Insert pitch: ")
        yaw = insert_number("Insert yaw: ")
        return [x, y, z, roll, pitch, yaw]

    def joints_request(self):
        j1 = insert_number("Insert joint 1: ")
        j2 = insert_number("Insert joint 2: ")
        j3 = insert_number("Insert joint 3: ")
        j4 = insert_number("Insert joint 4: ")
        j5 = insert_number("Insert joint 5: ")
        j6 = insert_number("Insert joint 6: ")
        return [j1, j2, j3, j4, j5, j6]

    def shift_pose_request(self):
        axis_error_msg = "HINT > x, y, z, roll, pitch, haw : 0, 1, 2, 3, 4, 5"
        request_msg = "Insert the number of the axis [0-5]: "
        axis = insert_number(request_msg, axis_error_msg, integer_value=True,
                             lower_bound=0, upper_bound=5)
        value = insert_number("Insert the value: ")
        return [axis, value]

    def set_arm_velocity_request(self):
        return [insert_number("Insert a percentage [1 - 100] > ", integer_value=True, lower_bound=1, upper_bound=100)]

    def learning_mode_request(self):
        return [1 if yn_input("Activate learning mode?[Y/N] > ") else 0]

    def gripper_request(self):
        gripper_id = insert_number(
            "Insert gripper id [tool_id_1 = 11] > ", integer_value=True, lower_bound=11, upper_bound=13)
        speed = insert_number(
            "Insert gripper speed [100 - 500] > ", integer_value=True, lower_bound=100, upper_bound=500)
        return [gripper_id, speed]

    def move_pose_JOINTS_request(self):
        x = insert_number("Insert x: ")
        y = insert_number("Insert y: ")
        z = insert_number("Insert z: ")
        return self.joint_calculator.joint_angles(x, y, z)

    ############################ AUTOMOVE ################################

    def automove_request(self):
        x = insert_number("Insert gripper x: ")
        y = insert_number("Insert gripper y: ")
        z = insert_number("Insert gripper z: ")
        return self.automove_fixed_space(x, y, z)

    def exec_routine(self, routine, params=[]):
        routine_index = self.routines.index(routine)
        self.add_cmd_pkt(self.make_packet_from_index(routine_index, params))
        return self.start_talk()

    def get_info(self):
        routine = "get_arm_pose"
        return self.exec_routine(routine)


    def scan_joint_delay(self, partitions, delay):
        routine = "move_joints"
        joint1 = JOINT1_MIN
        center = (JOINT1_MAX - JOINT1_MIN)/(2*partitions)
        params = self.current_joints
        for _ in range(partitions - 1):
            joint1 += center
            params[0] = joint1
            move_result = self.exec_routine(routine, params)
            rospy.sleep(delay)

    def scan_joint(self, partitions, partition):
        if partition not in range(0, partitions):
            return False
        routine = "move_joints"
        delta = (JOINT1_MAX - JOINT1_MIN)/(2*partitions)
        center = JOINT1_MIN + delta*(2*partition + 1)
        params = [center] + self.current_joints[1:]
        return self.exec_routine(routine, params)

    def scan_pose(self, partitions, partition, radius, z ):
        partition = int(partition)
        partitions = int(partitions)
        if partition not in range(1, partitions+1):
            return False
        sub_sectors = math.radians(360)/(2*partitions)
        sector = math.radians(180) - sub_sectors * (2*partition - 1)
        x = radius * math.cos(sector)
        y = radius * math.sin(sector)
        return self.automove_fixed_space(x, y, z)



    def move_scan(self, partitions, clockwise=True):
        routine = "move_joints"
        delta = (JOINT1_MAX - JOINT1_MIN)/partitions
        if clockwise:
            joint1 = self.current_joints[0] + delta
            if joint1 > JOINT1_MAX:
                joint1 = JOINT1_MAX
        else:
            joint1 = self.current_joints[0] - delta
            if joint1 < JOINT1_MIN:
                joint1 = JOINT1_MIN
        params = [joint1] + self.current_joints[1:]
        self.exec_routine(routine, params)

    def open_gripper(self, speed):
        routine = "open_gripper"
        params = [GRIPPER_ID, speed]
        return self.exec_routine(routine, params)

    def close_gripper(self, speed):
        routine = "close_gripper"
        params = [GRIPPER_ID, speed]
        return self.exec_routine(routine, params)

    def automove(self, x, y, z):
        # relative_target : z included
        pos_base = self.current_pose[0:3]
        angle = self.current_joints[0]
        x, y, z = homogeneous_transformation(pos_base, [x, y, z], angle)
        return self.automove_fixed_space(x, y, z)

    def automove_fixed_space(self, x, y, z):
        # z of gripper end
        z += GRIPPER_HEIGHT
        roll, pitch, yaw = AUTO_ROLL, AUTO_PITCH, yaw_angle(x, y)
        routine = "move_pose"
        params = [x, y, z, roll, pitch, yaw]
        move_result = self.exec_routine(routine, params)
        gripper_pose = [self.current_pose[0], self.current_pose[1], self.current_pose[2] -
                        GRIPPER_HEIGHT, self.current_pose[3], self.current_pose[4], self.current_pose[5]]
        return move_result, gripper_pose


########################## GENARAL MAIN ##############################


def run():
    rospack = rospkg.RosPack()
    path = os.path.join(rospack.get_path('robot_manager_pkg'), 'scripts')

    node_name = 'trajectory_sender_node'
    command_topic = 'robotic_trajectory'
    verbose_topic = 'robotic_trajectory_verbose'
    engine_topic = 'engine_topic'
    res_engine_topic = 'res_engine_topic'

    routines = 'settings/jetson_robot_routines.cfg'
    responses = 'settings/niryo_responses.cfg'
    user_interface_routines = 'settings/user_interface_commands.cfg'
    joint_configuration = 'settings/joint_configuration.cfg'
    demo_poses = 'settings/demo_poses.cfg'

    routines = os.path.join(path, routines)
    responses = os.path.join(path, responses)
    user_interface_routines = os.path.join(path, user_interface_routines)
    joint_configuration = os.path.join(path, joint_configuration)
    demo_poses = os.path.join(path, demo_poses)

    ros_ip = "10.42.0.243"
    ros_master_uri = "http://10.42.0.96:11311"

    robot = RobotManager(node_name, command_topic, verbose_topic, engine_topic,
                         res_engine_topic, routines, responses, ros_ip, ros_master_uri)
    user_loop = "Insert another command?[Y/N] > "
    robot.user_interface(user_interface_routines, user_loop)
    #rospy.spin()


if __name__ == '__main__':
    run()
