# Cognitive Robotics

This repo contains the code and documentation of the project realized for the Cognitive Robotics course, developed during my Master's Degree course in Computer Engineering.

# Robotic Grasping & Placing

This repo contains the ROS nodes for the Niryo One robotic arm control, including the Vision System, Control Logic, Path Planner and Motion Controller, organized in different workspaces, one for each subsystem (NVIDIA Jetson Nano, Niryo One's Raspberry Pi, Notebook).
