#!/usr/bin/env python3
import rospy
import numpy as np
import cv2 as cv
from std_msgs.msg import Int32MultiArray, Float32MultiArray
from sensor_msgs.msg import CompressedImage
import threading
import os

blank_image = np.zeros((480,640*2,3),np.uint8)
classNames = {0: 'background', 1 : 'red_ball', 2 : 'blue_ball', 3 : 'green_ball' , 4 : 'white_ball', 5: 'red_box', 6: 'blue_box', 7: 'green_box', 8: 'white_box'}


def image_callback(ros_data):
    #### direct conversion to CV2 ####
    np_arr = np.fromstring(ros_data.data, np.uint8)
    #image_np = cv2.imdecode(np_arr, cv2.CV_LOAD_IMAGE_COLOR
    image_np = cv.imdecode(np_arr, cv.IMREAD_COLOR) # OpenCV >= 3.0:


    cv.imshow('Image processed', image_np)
    rospy.loginfo(" Image received...")
    rospy.loginfo("\n\n#######################\n\n")
    cv.waitKey(1)


def listener():
    os.environ["ROS_IP"] = "10.42.0.1"
    os.environ["ROS_MASTER_URI"] = "http://10.42.0.96:11311"
    rospy.init_node('image_processed_listener')
    rospy.Subscriber("image_topic", CompressedImage, image_callback)
    rospy.loginfo(" IMAGE LISTENER INITIALIZED ")
    rospy.spin()


if __name__ == '__main__':
    listener()
