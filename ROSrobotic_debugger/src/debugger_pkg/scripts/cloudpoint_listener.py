#!/usr/bin/env python3
import rospy
import numpy as np
import cv2 as cv
from std_msgs.msg import Int32MultiArray, Float32MultiArray
from sensor_msgs.msg import CompressedImage
import threading
import os


classNames = {0: 'background', 1 : 'red_ball', 2 : 'blue_ball', 3 : 'green_ball' , 4 : 'white_ball', 5: 'red_box', 6: 'blue_box', 7: 'green_box', 8: 'white_box'}

"""
def net_callback(data):
    output = data.data
    size = len(output)
    blank_image[0:480,640:640*2] = np.zeros((480,640,3),np.uint8)
    if size != 0:
        for i in range(size//5):
            delta = (i-1)*5
            x = output[1+delta]
            y = output[2+delta]
            right = output[3+delta]
            bottom = output[4+delta]
            x_center = int(x+((right-x)/2))
            y_center = int(y+((bottom-y)/2))
            print("CLASS = {0}, X_CENTER = {1}, Y_CENTER = {2}".format(classNames[output[delta]],x_center,y_center))
    print("\n\n#######################\n\n")
"""


def coordinates_callback(data):
    coordinates = data.data
    size = len(coordinates)
    blank_image = np.zeros((480,640,3),np.uint8)
    print("LEN : " + str(size))
    if size != 0:
        for delta in range(0, size, 4):
            single_coordinates_string = "CLASS = {0} , X = {1:.3f} , Y = {2:.3f} , Z = {3:.3f}".format(classNames[coordinates[delta]],coordinates[delta+1],coordinates[delta+2],coordinates[delta+3])
            rospy.loginfo(single_coordinates_string)
            cv.putText(blank_image,single_coordinates_string,(10, 20 + 10*(delta+1)),cv.FONT_HERSHEY_SIMPLEX,0.5,(255,255,255))

    cv.imshow("COORDINATES",blank_image)
    cv.waitKey(1)
    rospy.loginfo("\n\n#######################\n\n")


def listener():
    os.environ["ROS_IP"] = "10.42.0.1"
    os.environ["ROS_MASTER_URI"] = "http://10.42.0.96:11311"
    rospy.init_node('cloudpoint_listener')
    rospy.Subscriber("cloudpoint_topic", Float32MultiArray, coordinates_callback)
    rospy.loginfo(" COORDINATES LISTENER INITIALIZED ")
    rospy.spin()


if __name__ == '__main__':
    listener()
